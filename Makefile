# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/22 17:53:00 by agauci-d          #+#    #+#              #
#    Updated: 2015/03/06 16:32:47 by agauci-d         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CC = gcc

SOURCE = ft_printf.c  ./sources/ft_flag_analyse.c ./sources/ft_conv.c\
		 ./sources/ft_init_flags.c ./sources/ft_init_tab.c  \
		 ./sources/ft_percent.c ./sources/ft_print_signed_number.c\
		 ./sources/ft_putnbr_ll.c ./sources/ft_ll_len.c ./sources/ft_d.c\
		 ./sources/ft_d_jz.c ./sources/ft_s.c ./sources/ft_print_chars.c\
		 ./sources//ft_c.c ./sources/ft_print_charc.c ./sources/ft_o.c\
		 ./sources/ft_print_unsigned_number.c ./sources/ft_conv_octal.c\
		 ./sources/ft_putnbr_ull.c ./sources/ft_ull_len.c ./sources/ft_ulltoa.c\
		 ./sources/ft_o_jz.c ./sources/ft_x.c ./sources/ft_conv_hexa.c \
		 ./sources/ft_u.c ./sources/ft_strjoin_c.c ./sources/ft_putstr_n.c \
		 ./sources/libft/ft_strlen.c \
		 ./sources/libft/ft_strdup.c \
		 ./sources/libft/ft_isdigit.c \
		 ./sources/libft/ft_putchar.c \
		 ./sources/libft/ft_putnbr.c \
		 ./sources/libft/ft_putstr.c \
		 ./sources/libft/ft_strjoin.c \
		 ./sources/libft/ft_strsub.c \
		 ./sources/libft/ft_strcmp.c \
		 ./sources/libft/ft_strcat.c \
		 ./sources/libft/ft_strncpy.c \
		 ./sources/libft/ft_strnew.c \
		 ./sources/ft_print_o_unsigned_number.c ./sources/ft_print_c.c\
		 ./sources/ft_print_s.c ./sources/ft_printf_conv_unicode.c\
		 ./sources/ft_cs.c ./sources/ft_wchar_len.c \
		 ./sources/ft_size_arg_null_s.c ./sources/ft_size_arg_null.c \
		 ./sources/ft_size.c

NOM = $(basename $(SOURCE))

OBJET = $(addsuffix .o, $(NOM))

FLAGS = -Wall -Werror -Wextra -I ./includes -I ./sources/libft/includes


HEADER = ft_printf.h

all: $(NAME)

$(NAME): $(OBJET)
		make -C ./sources/libft/
		ar -rsc $(NAME) $^
		echo "make libftprintf OK"

%.o: %.c
		$(CC) -o $@ -c $< $(FLAGS)

clean:
		make -C ./sources/libft/ clean
		/bin/rm -f $(OBJET)
		echo "clean libftprintf OK"

fclean: clean
		make -C ./sources/libft/ fclean
		/bin/rm -f $(NAME)
		echo "fclean libftptintf OK"

re: fclean all
		echo "re libftprintf OK"

.PHONY: all clean fclean re
