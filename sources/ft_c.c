/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_c.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 16:46:00 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/06 16:46:01 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_l_c(va_list ap, t_env *env)
{
	wint_t	arg;
	int		i;

	arg = (wchar_t)va_arg(ap, wint_t);
	ft_printf_conv_unicode(arg, env);
	i = ft_print_c(env);
	free(env->quest);
	return (i);
}

static char	ft_uchar(va_list ap)
{
	int		arg;
	char	arg2;

	arg = va_arg(ap, int);
	arg2 = (char)arg;
	return (arg2);
}

int			ft_c(va_list ap, t_env *env)
{
	char	arg;
	int		i;

	if (env->l || env->spec == 'C')
		i = ft_l_c(ap, env);
	else
	{
		arg = ft_uchar(ap);
		i = ft_print_charc(arg, env);
	}
	return (i);
}
