/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_size_arg_null.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 15:57:05 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/06 15:57:26 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_size_arg_null(int i, char *c, int j)
{
	while (i < j)
	{
		write(1, c, ft_strlen(c));
		i += ft_strlen(c);
	}
	return (i);
}
