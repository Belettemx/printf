/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_o.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/23 11:30:16 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/06 15:17:19 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static unsigned long long int	ft_ll_signed(va_list ap)
{
	unsigned long long int arg;

	arg = va_arg(ap, unsigned long long int);
	return (arg);
}

static unsigned long int		ft_l_signed(va_list ap)
{
	unsigned long int arg;

	arg = va_arg(ap, unsigned long int);
	return (arg);
}

static unsigned char			ft_hh_signed(va_list ap)
{
	unsigned char			arg;

	arg = va_arg(ap, unsigned int);
	return (arg);
}

static unsigned short			ft_h_signed(va_list ap)
{
	unsigned short arg;

	arg = va_arg(ap, unsigned int);
	return (arg);
}

int								ft_o(va_list ap, t_env *env)
{
	unsigned long long int	arg;
	char					*dst;
	int						i;

	i = 0;
	if (env->ll && env->spec != 'O')
		arg = ft_ll_signed(ap);
	else if (env->l || env->spec == 'O')
		arg = (unsigned long long int)ft_l_signed(ap);
	else if (env->hh)
		arg = (unsigned long long int)ft_hh_signed(ap);
	else if (env->h)
		arg = (unsigned long long int)ft_h_signed(ap);
	else if (env->j || env->z)
		arg = ft_ojz(ap, env);
	else
	{
		arg = va_arg(ap, unsigned int);
		arg = (unsigned long long int)arg;
	}
	dst = ft_conv_octal(arg, env);
	i = ft_print_unsigned_o_number(dst, env);
	return (i);
}
