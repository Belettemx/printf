/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 16:45:48 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/06 16:45:50 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_conv(va_list ap, t_env *env, t_tab *tab)
{
	int c;

	c = (int)env->spec;
	return (tab->tab_ft_conv[c](ap, env));
}
