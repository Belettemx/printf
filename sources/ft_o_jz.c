/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_o_jz.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 16:43:23 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/06 16:43:25 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned long long int ft_ojz(va_list ap, t_env *env)
{
	unsigned long long int	arg;

	arg = 0;
	if (env->j)
	{
		arg = (unsigned long long int)va_arg(ap, uintmax_t);
	}
	else if (env->z)
	{
		arg = va_arg(ap, size_t);
		arg = (unsigned long long int)arg;
	}
	return (arg);
}
