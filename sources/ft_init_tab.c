/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_funct_conv.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 18:03:07 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/04 18:00:30 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_init_tab_conv(t_tab *tab)
{
	int i;

	i = 0;
	while (i < 126)
	{
		tab->tab_ft_conv[i] = &ft_percent;
		i++;
	}
	tab->tab_ft_conv['s'] = &(ft_s);
	tab->tab_ft_conv['S'] = &(ft_s);
	tab->tab_ft_conv['p'] = &(ft_x);
	tab->tab_ft_conv['d'] = &ft_d;
	tab->tab_ft_conv['D'] = &(ft_d);
	tab->tab_ft_conv['i'] = &(ft_d);
	tab->tab_ft_conv['o'] = &(ft_o);
	tab->tab_ft_conv['O'] = &(ft_o);
	tab->tab_ft_conv['u'] = &(ft_u);
	tab->tab_ft_conv['U'] = &(ft_u);
	tab->tab_ft_conv['x'] = &(ft_x);
	tab->tab_ft_conv['X'] = &(ft_x);
	tab->tab_ft_conv['c'] = &(ft_c);
	tab->tab_ft_conv['C'] = &(ft_c);
	tab->tab_ft_conv['%'] = &ft_percent;
}

void		ft_init_tab(t_tab *tab)
{
	ft_init_tab_conv(tab);
}
